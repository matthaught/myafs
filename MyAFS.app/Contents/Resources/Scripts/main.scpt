FasdUAS 1.101.10   ��   ��    k             l     ��  ��    4 . Get AFS kerberos token and open finder window     � 	 	 \   G e t   A F S   k e r b e r o s   t o k e n   a n d   o p e n   f i n d e r   w i n d o w   
  
 l     ��������  ��  ��        l     ��������  ��  ��        l     ��������  ��  ��        l     ����  Q       ��  k    
       l   ��  ��    ; 5 see if we already have a principal and use that user     �   j   s e e   i f   w e   a l r e a d y   h a v e   a   p r i n c i p a l   a n d   u s e   t h a t   u s e r   ��  r    
    I   �� ��
�� .sysoexecTEXT���     TEXT  m       �     � / u s r / b i n / k l i s t   |   / u s r / b i n / g r e p   r i n c i p a l :   |   / u s r / b i n / s e d   ' s / . * r i n c i p a l :   / / ; s /   * @ . * / / ; '��    o      ���� 0 user USER��    R      ������
�� .ascrerr ****      � ****��  ��  ��  ��  ��     ! " ! l   1 #���� # Z    1 $ %���� $ =    & ' & o    ���� 0 user USER ' m     ( ( � ) )   % Q    - * +�� * k    $ , ,  - . - l   �� / 0��   /   use local user name    0 � 1 1 (   u s e   l o c a l   u s e r   n a m e .  2�� 2 r    $ 3 4 3 I   "�� 5��
�� .fndrgstl****    ��� **** 5 m     6 6 � 7 7  U S E R��   4 o      ���� 0 user USER��   + R      ������
�� .ascrerr ****      � ****��  ��  ��  ��  ��  ��  ��   "  8 9 8 l     ��������  ��  ��   9  : ; : l  2 � <���� < Q   2 � = > ? = k   5 = @ @  A B A l  5 5�� C D��   C = 7 check USER to see if they have a homeDirectory in ldap    D � E E n   c h e c k   U S E R   t o   s e e   i f   t h e y   h a v e   a   h o m e D i r e c t o r y   i n   l d a p B  F�� F r   5 = G H G I   5 ;�� I���� $0 gethomedirectory getHomeDirectory I  J�� J o   6 7���� 0 user USER��  ��   H o      ���� 0 home HOME��   > R      ������
�� .ascrerr ****      � ****��  ��   ? k   E � K K  L M L l  E E�� N O��   N 2 , homeDirectory not found so ask for username    O � P P X   h o m e D i r e c t o r y   n o t   f o u n d   s o   a s k   f o r   u s e r n a m e M  Q R Q l  E E�� S T��   S \ Vset USER to text returned of (display dialog "Enter your unity ID:" default answer "")    T � U U � s e t   U S E R   t o   t e x t   r e t u r n e d   o f   ( d i s p l a y   d i a l o g   " E n t e r   y o u r   u n i t y   I D : "   d e f a u l t   a n s w e r   " " ) R  V W V r   E ` X Y X n   E ^ Z [ Z 1   Z ^��
�� 
ttxt [ l  E Z \���� \ I  E Z�� ] ^
�� .sysodlogaskr        TEXT ] m   E F _ _ � ` ` ( E n t e r   y o u r   u n i t y   I D : ^ �� a b
�� 
dtxt a m   G H c c � d d   b �� e��
�� 
disp e l  I T f���� f I  I T�� g h
�� .sysorpthalis        TEXT g m   I J i i � j j  a p p l e t . i c n s h �� k��
�� 
in B k l  K P l���� l I  K P�� m��
�� .earsffdralis        afdr m  f   K L��  ��  ��  ��  ��  ��  ��  ��  ��   Y o      ���� 0 user USER W  n�� n Q   a � o p q o r   d l r s r I   d j�� t���� $0 gethomedirectory getHomeDirectory t  u�� u o   e f���� 0 user USER��  ��   s o      ���� 0 home HOME p R      �� v��
�� .ascrerr ****      � **** v o      ���� 0 theerror theError��   q k   t � w w  x y x I  t ��� z��
�� .sysodlogaskr        TEXT z b   t } { | { b   t y } ~ } m   t w   � � � . E r r o r :   T h e   U n i t y   u s e r   " ~ o   w x���� 0 user USER | m   y | � � � � �  "   w a s   n o t   f o u n d��   y  ��� � R   � ����� �
�� .ascrerr ****      � ****��   � �� ���
�� 
errn � m   � ���������  ��  ��  ��  ��   ;  � � � l     ��������  ��  ��   �  � � � l  � � ����� � Q   � � � � � � k   � � � �  � � � l  � ��� � ���   � 4 . see if we have afs token that has not expired    � � � � \   s e e   i f   w e   h a v e   a f s   t o k e n   t h a t   h a s   n o t   e x p i r e d �  ��� � r   � � � � � I  � ��� ���
�� .sysoexecTEXT���     TEXT � m   � � � � � � � z / u s r / b i n / k l i s t   |   / u s r / b i n / g r e p   a f s   |   / u s r / b i n / g r e p   - v   E x p i r e d��   � o      ���� 0 token TOKEN��   � R      ������
�� .ascrerr ****      � ****��  ��   � r   � � � � � m   � � � � � � �   � o      ���� 0 token TOKEN��  ��   �  � � � l  � � ����� � Z   � � � ��� � � >  � � � � � o   � ����� 0 token TOKEN � m   � � � � � � �   � k   � � � �  � � � l  � ��� � ���   � . ( valid afs token found, just open finder    � � � � P   v a l i d   a f s   t o k e n   f o u n d ,   j u s t   o p e n   f i n d e r �  ��� � I   � ��� ����� 0 
openfinder 
openFinder �  ��� � o   � ����� 0 home HOME��  ��  ��  ��   � k   � � � �  � � � l  � ��� � ���   � ; 5 no valid afs token found, kinit and then open finder    � � � � j   n o   v a l i d   a f s   t o k e n   f o u n d ,   k i n i t   a n d   t h e n   o p e n   f i n d e r �  � � � r   � � � � � I   � ��� ����� 0 getkerberos getKerberos �  ��� � o   � ����� 0 user USER��  ��   � o      ���� 0 user USER �  ��� � I   � ��� ����� 0 
openfinder 
openFinder �  ��� � o   � ����� 0 home HOME��  ��  ��  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �������  ��  �   �  � � � l     �~�}�|�~  �}  �|   �  � � � l     �{ � ��{   � 4 . function for looking up homeDirectory in ldap    � � � � \   f u n c t i o n   f o r   l o o k i n g   u p   h o m e D i r e c t o r y   i n   l d a p �  � � � i      � � � I      �z ��y�z $0 gethomedirectory getHomeDirectory �  ��x � o      �w�w 0 myuser myUSER�x  �y   � k     ' � �  � � � Q      � ��v � r     � � � I   �u ��t
�u .sysoexecTEXT���     TEXT � b     � � � b     � � � m     � � � � � � l d a p s e a r c h   - h   l d a p . n c s u . e d u   - Z   - x   - b   ' o u = a c c o u n t s , d c = n c s u , d c = e d u '   ' u i d = � o    �s�s 0 myuser myUSER � m     � � � � � � '   h o m e D i r e c t o r y   |   g r e p   h o m e D i r e c t o r y :   |   s e d   ' s / h o m e D i r e c t o r y :   / / '�t   � o      �r�r 0 home HOME � R      �q�p�o
�q .ascrerr ****      � ****�p  �o  �v   �  ��n � Z    ' � ��m � � >    � � � o    �l�l 0 home HOME � m     � � � � �   � L      � � o    �k�k 0 home HOME�m   � R   # '�j�i�h
�j .ascrerr ****      � ****�i  �h  �n   �  � � � l     �g�f�e�g  �f  �e   �  � � � l     �d�c�b�d  �c  �b   �  � � � l     �a � ��a   � * $ function for getting kerberos token    � � � � H   f u n c t i o n   f o r   g e t t i n g   k e r b e r o s   t o k e n �  � � � i     � � � I      �` ��_�` 0 getkerberos getKerberos �  ��^ � o      �]�] 0 myuser myUSER�^  �_   � k     � � �  � � � Q     c � � � � k     � �    l   �\�\   ? 9 try a kinit, 10.7+ will not prompt for password and fail    � r   t r y   a   k i n i t ,   1 0 . 7 +   w i l l   n o t   p r o m p t   f o r   p a s s w o r d   a n d   f a i l �[ I   �Z�Y
�Z .sysoexecTEXT���     TEXT b     b    	
	 m     �  / u s r / b i n / k i n i t  
 o    �X�X 0 myuser myUSER m     �  @ E O S . N C S U . E D U�Y  �[   � R      �W�V�U
�W .ascrerr ****      � ****�V  �U   � k    c  l   �T�T   R L 10.7 does not have gui for kinit so we have to manually ask for information    � �   1 0 . 7   d o e s   n o t   h a v e   g u i   f o r   k i n i t   s o   w e   h a v e   t o   m a n u a l l y   a s k   f o r   i n f o r m a t i o n �S Q    c k    K  l   �R�R   C = ask for password and try kinit with existing myUSER variable    � z   a s k   f o r   p a s s w o r d   a n d   t r y   k i n i t   w i t h   e x i s t i n g   m y U S E R   v a r i a b l e   l   �Q!"�Q  ! ~ xset forget to text returned of (display dialog "AFS requires your Unity password:" default answer "" with hidden answer)   " �## � s e t   f o r g e t   t o   t e x t   r e t u r n e d   o f   ( d i s p l a y   d i a l o g   " A F S   r e q u i r e s   y o u r   U n i t y   p a s s w o r d : "   d e f a u l t   a n s w e r   " "   w i t h   h i d d e n   a n s w e r )  $%$ r    2&'& n    0()( 1   , 0�P
�P 
ttxt) l   ,*�O�N* I   ,�M+,
�M .sysodlogaskr        TEXT+ m    -- �.. B A F S   r e q u i r e s   y o u r   U n i t y   p a s s w o r d :, �L/0
�L 
dtxt/ m    11 �22  0 �K34
�K 
disp3 l   &5�J�I5 I   &�H67
�H .sysorpthalis        TEXT6 m    88 �99  a p p l e t . i c n s7 �G:�F
�G 
in B: l   ";�E�D; I   "�C<�B
�C .earsffdralis        afdr<  f    �B  �E  �D  �F  �J  �I  4 �A=�@
�A 
htxt= m   ' (�?
�? boovtrue�@  �O  �N  ' o      �>�> 
0 forget  % >�=> t   3 K?@? I  7 J�<A�;
�< .sysoexecTEXT���     TEXTA b   7 FBCB b   7 BDED b   7 @FGF b   7 <HIH m   7 :JJ �KK  / b i n / e c h o   'I o   : ;�:�: 
0 forget  G m   < ?LL �MM R '   |   / u s r / b i n / k i n i t   - - p a s s w o r d - f i l e = S T D I N  E o   @ A�9�9 0 myuser myUSERC m   B ENN �OO  @ E O S . N C S U . E D U�;  @ m   3 6�8�8 �=   R      �7P�6
�7 .ascrerr ****      � ****P o      �5�5 0 theerror theError�6   k   S cQQ RSR I  S X�4T�3
�4 .sysodlogaskr        TEXTT o   S T�2�2 0 theerror theError�3  S U�1U R   Y c�0�/V
�0 .ascrerr ****      � ****�/  V �.W�-
�. 
errnW m   ] `�,�,���-  �1  �S   � XYX Q   d �Z[�+Z t   g y\]\ I  k x�*^�)
�* .sysoexecTEXT���     TEXT^ b   k t_`_ b   k paba m   k ncc �dd ( / u s r / b i n / k s w i t c h   - p  b o   n o�(�( 0 myuser myUSER` m   p see �ff  @ E O S . N C S U . E D U�)  ] m   g j�'�' [ R      �&�%�$
�& .ascrerr ****      � ****�%  �$  �+  Y ghg Q   � �ijki t   � �lml I  � ��#n�"
�# .sysoexecTEXT���     TEXTn m   � �oo �pp ^ / u s r / b i n / a k l o g   - c   e o s   b p   u n i t y   - k   E O S . N C S U . E D U  �"  m m   � ��!�! j R      � q�
�  .ascrerr ****      � ****q o      �� 0 theerror theError�  k k   � �rr sts I  � ��u�
� .sysodlogaskr        TEXTu o   � ��� 0 theerror theError�  t v�v R   � ���w
� .ascrerr ****      � ****�  w �x�
� 
errnx m   � ������  �  h yzy l  � ��{|�  { 5 / return user so it may be used outside function   | �}} ^   r e t u r n   u s e r   s o   i t   m a y   b e   u s e d   o u t s i d e   f u n c t i o nz ~�~ L   � � o   � ��� 0 myuser myUSER�   � ��� l     ����  �  �  � ��� l     ����  �  �  � ��� l     ��
�	�  �
  �	  � ��� l     ����  � 3 - function for opening afs directory in finder   � ��� Z   f u n c t i o n   f o r   o p e n i n g   a f s   d i r e c t o r y   i n   f i n d e r� ��� i    ��� I      ���� 0 
openfinder 
openFinder� ��� o      �� 0 myhome myHOME�  �  � Q     6���� O    -��� k    ,�� ��� I   �� ��
� .miscactvnull��� ��� obj �   ��  � ���� Q    ,����� t    #��� k    "�� ��� I   �����
�� .aevtodocnull  �    alis� c    ��� o    ���� 0 myhome myHOME� m    ��
�� 
psxf��  � ���� r    "��� m    ��
�� ecvwicnv� l     ������ n      ��� 1    !��
�� 
pvew� l   ������ 4   ���
�� 
brow� m    ���� ��  ��  ��  ��  ��  � m    ���� � R      ������
�� .ascrerr ****      � ****��  ��  ��  ��  � m    ���                                                                                  MACS  alis    t  Macintosh HD               ��{�H+     2
Finder.app                                                      ��\\M        ����  	                CoreServices    ����      �\��       2   ,   +  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  � R      ������
�� .ascrerr ****      � ****��  ��  �  �       
��������� �����  � ������������������ $0 gethomedirectory getHomeDirectory�� 0 getkerberos getKerberos�� 0 
openfinder 
openFinder
�� .aevtoappnull  �   � ****�� 0 user USER�� 0 home HOME�� 0 token TOKEN��  � �� ����������� $0 gethomedirectory getHomeDirectory�� ����� �  ���� 0 myuser myUSER��  � ������ 0 myuser myUSER�� 0 home HOME�  � ������� �
�� .sysoexecTEXT���     TEXT��  ��  �� ( �%�%j E�W X  hO�� �Y )jh� �� ����������� 0 getkerberos getKerberos�� ����� �  ���� 0 myuser myUSER��  � �������� 0 myuser myUSER�� 
0 forget  �� 0 theerror theError� ������-��1��8����������������JLN������ceo
�� .sysoexecTEXT���     TEXT��  ��  
�� 
dtxt
�� 
disp
�� 
in B
�� .earsffdralis        afdr
�� .sysorpthalis        TEXT
�� 
htxt�� 
�� .sysodlogaskr        TEXT
�� 
ttxt�� �� 0 theerror theError
�� 
errn������ � �%�%j W VX   9������)j l �e� a ,E�Oa na �%a %�%a %j oW X  �j O)a a lhO a na �%a %j oW X  hO a na j oW X  �j O)a a lhO�� ������������� 0 
openfinder 
openFinder�� ����� �  ���� 0 myhome myHOME��  � ���� 0 myhome myHOME� 
�������������������
�� .miscactvnull��� ��� obj �� 
�� 
psxf
�� .aevtodocnull  �    alis
�� ecvwicnv
�� 
brow
�� 
pvew��  ��  �� 7 /� '*j O �n��&j O�*�k/�,FoW X  	hUW X  	h� �����������
�� .aevtoappnull  �   � ****� k     ���  ��  !��  :��  ���  �����  ��  ��  � ���� 0 theerror theError�   �������� ( 6������ _�� c�� i��������������  ����� ��� � �����
�� .sysoexecTEXT���     TEXT�� 0 user USER��  ��  
�� .fndrgstl****    ��� ****�� $0 gethomedirectory getHomeDirectory�� 0 home HOME
�� 
dtxt
�� 
disp
�� 
in B
�� .earsffdralis        afdr
�� .sysorpthalis        TEXT�� 
�� .sysodlogaskr        TEXT
�� 
ttxt�� 0 theerror theError
�� 
errn������ 0 token TOKEN�� 0 
openfinder 
openFinder�� 0 getkerberos getKerberos�� � �j E�W X  hO��   �j E�W X  hY hO *�k+ E�W NX  ������)j l a  a ,E�O *�k+ E�W X  a �%a %j O)a a lhO a j E` W X  a E` O_ a  *�k+ Y *�k+ E�O*�k+ � ���  d m h a u g h t� ��� H / a f s / u n i t y . n c s u . e d u / u s e r s / d / d m h a u g h t��   ascr  ��ޭ